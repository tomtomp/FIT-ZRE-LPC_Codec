/**
 * @file PackedBitStream.cpp
 * @author Tomas Polasek
 * @brief Bit packing helper class.
 */

#include "PackedBitStream.h"

namespace Util
{
    PackedBitStream::PackedBitStream()
    { reset(); }

    PackedBitStream::PackedBitStream(std::istream &input, bool canPreallocate)
    { loadFrom(input, canPreallocate); }

    void PackedBitStream::loadFrom(std::istream &input, bool canPreallocate)
    {
        reset();
        Util::readIntoVector(input, mData, canPreallocate);
        mTotalChunks = mData.size();
        mLastBits = CHUNK_BITS;
    }

    void PackedBitStream::reset()
    {
        mData.clear();
        mCurrentChunk = 0;
        mCurrentBit = 0;
        mTotalChunks = 0;
        mLastBits = 0;
    }

    void PackedBitStream::seekStart()
    {
        mCurrentChunk = 0;
        mCurrentBit = 0;
    }

    void PackedBitStream::seekEnd()
    {
        // Last block.
        mCurrentChunk = mTotalChunks - 1;
        // First unused bit.
        mCurrentBit = mLastBits;
    }

    void PackedBitStream::allocateAtLeast(size_t numChunks)
    {
        std::size_t totalChunks = nextMultipleOfChunks(numChunks);
        mData.resize(mData.size() + totalChunks);
    }

    void PackedBitStream::ensureSpace(std::size_t bits)
    {
        if (availableBits() < bits)
        {
            allocateAtLeast(bits / CHUNK_BITS + 1);
        }
    }

    std::size_t PackedBitStream::availableBits() const
    {
        std::size_t chunks{mData.size() - mTotalChunks};
        std::size_t remainingBits{CHUNK_BITS - mLastBits};

        return chunks * CHUNK_BITS + (chunks == 0 ? 0 : remainingBits);
    }

    std::size_t PackedBitStream::availableUsedBits() const
    {
        std::size_t chunks{mTotalChunks - mCurrentChunk - 1};
        std::size_t remainingBits{CHUNK_BITS - mCurrentBit};

        return chunks * CHUNK_BITS + remainingBits;
    }

    std::size_t PackedBitStream::availableUnusedBits() const
    {
        std::size_t chunks{mData.size() - mCurrentChunk};
        std::size_t remainingBits{CHUNK_BITS - mCurrentBit};

        return chunks * CHUNK_BITS + remainingBits;
    }
}
