/**
 * @file FirFilter.h
 * @author Tomas Polasek
 * @brief FIR filter wrapper.
 */

#ifndef ZRE_LPC_CODEC_FIRFILTER_H
#define ZRE_LPC_CODEC_FIRFILTER_H

#include "Types.h"
#include "Util.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    /// FIR filter wrapper class.
    class FirFilter
    {
    public:
        /**
         * Initialize filter with given coefficients.
         * @param coeffs List of filter coefficients.
         */
        FirFilter(const std::vector<FpSample> &coeffs);

        /**
         * Initialize filter with given order.
         * Coefficients will be set to 0.
         * @param order Order of the filter.
         */
        FirFilter(std::size_t order);

        /// Cleanup resources.
        ~FirFilter() = default;

        /**
         * Set coefficients of this filter.
         * Resets working memory, if the number of coefficients changes!
         */
        void setCoeffs(const std::vector<FpSample> &coeffs);

        /// Reset values in the work buffer.
        void resetWorkBuffer();

        /// Get order of this filter.
        std::size_t order() const
        { return mCoeffs.size(); }

        /**
         * Filter given input using this filter. Returns filtered
         * input, which has the length of the input vector minus
         * the order of this filter.
         * @param input Input data.
         * @return Returns filtered input vector with the length
         *   of the input one minus the order of this filter.
         */
        std::vector<FpSample> filter(const std::vector<FpSample> &input);

        /**
         * Filter given input using this filter. Returns filtered
         * input, which has the length of the input vector minus
         * the order of this filter.
         * This variant does not clear the working memory of previous
         * calls.
         * @param input Input data.
         * @return Returns filtered input vector with the length
         *   of the input.
         */
        std::vector<FpSample> filterNoReset(const std::vector<FpSample> &input);

        /**
         * Filter given input using this filter. Returns filtered
         * input, which has the length of the input vector minus
         * the order of this filter.
         * This variant does not clear the working memory of previous
         * calls.
         * @param input Input data.
         * @return Returns filtered input vector with the length
         *   of the input.
         */
        std::vector<FpSample> filterNoResetAdd(const std::vector<FpSample> &input);

        std::vector<FpSample> calcErrors(const std::vector<FpSample> &signal);
    private:
        /**
         * Filter given input using this filter. Returns filtered
         * input, which has the length of the input vector minus
         * the order of this filter.
         * @param input Input data.
         * @return Returns filtered input vector with the length
         *   of the input one minus the order of this filter.
         * @warning Does not check anything!
         */
        std::vector<FpSample> doFilter(const std::vector<FpSample> &input);

        /**
         * Filter given input using this filter. Returns filtered
         * input, which has the length of the input vector minus
         * the order of this filter.
         * @param input Input data.
         * @return Returns filtered input vector with the length
         *   of the input.
         * @warning Does not check anything!
         */
        std::vector<FpSample> doFilterNoReset(const std::vector<FpSample> &input);

        /**
         * Filter given input using this filter. Returns filtered
         * input, which has the length of the input vector minus
         * the order of this filter.
         * @param input Input data.
         * @return Returns filtered input vector with the length
         *   of the input.
         * @warning Does not check anything!
         */
        std::vector<FpSample> doFilterNoResetAdd(const std::vector<FpSample> &input);

        /// List of filter coefficients.
        std::vector<FpSample> mCoeffs;
        /// Work buffer.
        std::vector<FpSample> mWork;
    protected:
    }; // class FirFilter
} // namespace Util

#endif //ZRE_LPC_CODEC_FIRFILTER_H
