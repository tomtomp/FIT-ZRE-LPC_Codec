/**
 * @file Config.h
 * @author Tomas Polasek
 * @brief Configuration of codec parameters.
 */

#ifndef ZRE_LPC_CODEC_CONFIG_H
#define ZRE_LPC_CODEC_CONFIG_H

#include "Types.h"

/// Preemphasis coefficient.
static constexpr float ALPHA{0.2};
/// LPC order.
static constexpr std::size_t LPC_ORDER{10};
/// Error under-sampling stride.
static constexpr std::size_t ERROR_UNDERSAMPLE{1};
/// Multiplier for the LPC coefficients maximum.
static constexpr FpSample LPC_MAX_MULTIPLIER{32000.0f};
/// Multiplier for the errors maximum.
static constexpr FpSample ERR_MAX_MULTIPLIER{1.0f};
/// Number of bits used for coding the LPC coefficients.
static constexpr std::size_t LPC_BITS{8};
/// Quantization multiplier for LPC coefficients.
static constexpr FpSample LPC_QUANT{(1 << LPC_BITS) / 2.0f - 0.5f};
/// Number of bits used for coding the errors.
static constexpr std::size_t ERR_BITS{3};
/// Quantization multiplier for errors.
static constexpr FpSample ERR_QUANT{(1 << ERR_BITS) / 2.0f - 0.5f};
/// How much should the input signal be undersampled?
static constexpr std::size_t SIGNAL_UNDERSAMPLE{2};
/// Size of one frame.
static constexpr std::size_t FRAME_SIZE{120};

#endif //ZRE_LPC_CODEC_CONFIG_H
