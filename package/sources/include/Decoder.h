/**
 * @file Decoder.h
 * @author Tomas Polasek
 * @brief Main file for the decoding program.
 */

#ifndef ZRE_LPC_CODEC_DECODER_H
#define ZRE_LPC_CODEC_DECODER_H

#include "Types.h"
#include "Util.h"
#include "Config.h"

#include "PackedBitStream.h"
#include "LpcDecoder.h"

/// Application class.
class DecoderApp
{
public:
    /**
     * Initialize the decoding application.
     * @param argc Number of command line arguments.
     * @param argv Command line argument vector.
     */
    DecoderApp(int argc, const char *argv[]);

    /**
     * Main loop of the application.
     * @return Returns application return code.
     */
    int run();
private:
    enum
    {
        /// Command line argument location for input filename.
            INPUT_ARG_LOC = 1,
        /// Command line argument location for output filename.
            OUTPUT_ARG_LOC = 2,
        /// Minimal number of command line arguments.
            MIN_ARGS
    };

    /// Help message for the application.
    static constexpr const char *HELP_MSG{"Usage: \n"
                                              "\t./zre_code INPUT_FILE OUTPUT_FILE\n"
                                              "Description:\n"
                                              "\tINPUT_FILE: Location of the input file, format signed 16b numbers. Use \"-\" for stdin.\n"
                                              "\tOUTPUT_FILE: Location of the encoded output file. Use \"-\" for stdout."};

    /// Process command line arguments.
    void processArgs(int argc, const char *argv[]);

    /**
     * Read data from given filename.
     * @param filename Filename or "-" for stdin.
     * @param pbs Packed bit stream target.
     */
    void readData(std::string &filename, Util::PackedBitStream &pbs);

    /**
     * Write data to given filename.
     * @param data Data vector.
     * @param filename Filename or "-" for stdout.
     */
    void writeData(const std::vector<Sample> &data, std::string &filename);

    /// Print the help menu.
    void printHelp();

    /// Output filename.
    std::string mOutputFilename;
    /// Input packed bit stream.
    Util::PackedBitStream mPbs;
protected:
}; // class DecoderApp

#endif //ZRE_LPC_CODEC_DECODER_H
