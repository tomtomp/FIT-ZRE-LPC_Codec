/**
 * @file LpcCoder.h
 * @author Tomas Polasek
 * @brief LPC coding class.
 */

#ifndef ZRE_LPC_CODEC_LPCCODER_H
#define ZRE_LPC_CODEC_LPCCODER_H

#include "Types.h"
#include "Util.h"
#include "LpcDecoder.h"

/// Namespace containing linear predictive coding helper functions and classes.
namespace Lpc
{
    /// Abstraction for coding data using Linear Predictive coding.
    class LpcCoder
    {
    public:
        /**
         * Initializer the coder with given parameters.
         * @param order Order of the LPC filter.
         */
        LpcCoder(std::size_t order);

        /// Free all resources.
        ~LpcCoder() = default;

        /**
         * Encode given input using the Linear Predictive Coding.
         * Calculates LPC coefficients, which can be used to
         * reconstruct the original signal. Function also
         * returns gain.
         * @param input Input signal values.
         * @param coeffs LPC coefficients output.
         * @return Gain value.
         */
        FpSample encode(const std::vector<FpSample> &input,
                        std::vector<FpSample> &coeffs);
    private:
        /**
         * Calculate LPC coefficients using Levinson-Durbin method.
         * @param correls Input vector containing auto-correlation coefficients.
         * @param order Target order of the LPC filter.
         * @param lastError Output for last error value.
         * @return Returns vector containing LPC coefficients.
         */
        std::vector<FpSample> levinsonDurbinLpc(const std::vector<FpSample> &correls,
                                                std::size_t order,
                                                FpSample &lastError);

        /// Order of the LPC filter.
        std::size_t mOrder;
    protected:
    }; // class LpcCoder
} // namespace Lpc

#endif //ZRE_LPC_CODEC_LPCCODER_H
