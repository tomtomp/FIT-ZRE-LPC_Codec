     LPC kodek do 16 kbit/s
    Tomáš Polášek (xpolas34)
    ========================

Tento projekt byl vytvořen do předmětu Zpracování Řeči na Faktultě Informačních Technologií v Brně.

Cílem bylo vytvořit řečový kodek, který dosahuje bitového toku max. 16 kbit/s. Dalším pořadavkem bylo použití alespoň jednoho výpočtu LPC koeficientů.

Obsah archivu
=============

Tento archiv obsahuje následující soubory a složky: 
 * Adresář sources/ : Obsahuje zdrojové kódy kodeku v jazyce C++.
 * Adresář execs/ : Obsahuje přeložený kodér (zre_code) a dekodér (zre_decode), funkční na školním serveru Merlin.
 * Soubor xpolas34.pdf : Dokumentace k projektu.
 * Adresář testing/ : Obsahuje jeden testovací zvukový soubor (test.raw), Matlab soubor, který obsahuje implementaci logaritmického spektrálního zkreslení (log_spec_dist.m) a ukázku spuštění kodeku s poslechem pomocí ffmpeg (command.txt)

Překlad
=======

Překlad lze provést následující posloupností příkazů: 
> cd proj_root/sources/ && mkdir build && cd build
> cmake ..
> make -j4
> cd ../bin

Výsledné spustitelné soubory lze potom nalézt v adresáři sources/bin/ .

