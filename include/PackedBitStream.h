/**
 * @file PackedBitStream.h
 * @author Tomas Polasek
 * @brief Bit packing helper class.
 */

#ifndef ZRE_LPC_CODEC_PACKEDBITSTREAM_H
#define ZRE_LPC_CODEC_PACKEDBITSTREAM_H

#include "Types.h"
#include "Util.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    class PackedBitStream
    {
    public:
        /// Smallest allocatable resource.
        using Chunk = uint8_t;

        /// Number of bits per one byte.
        static constexpr std::size_t BITS_PER_BYTE{8u};
        /// Size of on unit in the data buffer.
        static constexpr std::size_t CHUNK_BYTES{sizeof(Chunk)};
        /// Number of bits per one chunk in the data buffer.
        static constexpr std::size_t CHUNK_BITS{CHUNK_BYTES * BITS_PER_BYTE};
        /// Number of chunks allocated, when space runs out.
        static constexpr std::size_t ALLOC_SIZE{128};

        // Initialize empty bit stream.
        PackedBitStream();

        /**
         * Load data from given input stream.
         * @param input Stream from which the data is loaded.
         * @param canPreallocate Can the output buffer be
         *   pre-allocated? Should be set to false for std::cin.
         */
        PackedBitStream(std::istream &input, bool canPreallocate);

        // Free resources.
        ~PackedBitStream() = default;

        /**
         * Load data from given input stream.
         * @param input Stream from which the data is loaded.
         * @param canPreallocate Can the output buffer be
         *   pre-allocated? Should be set to false for std::cin.
         * @warning Resets the data before loading!
         */
        void loadFrom(std::istream &input, bool canPreallocate);

        /// Reset the internal structure to default empty state.
        void reset();

        /// Move index to the start of the data buffer.
        void seekStart();

        /// Move index to the end of the data buffer, to the first unused bit.
        void seekEnd();

        /**
         * Read value from the current position.
         * Used the full size of the type.
         * @tparam T Type which should be read.
         * @return Returns value read.
         * @warning Moves the current pointer by
         *   number of read bits.
         * @throws std::runtime_error when there
         *   are not enough bits left.
         */
        template <typename T>
        T readVal();

        /**
         * Read value from the current position.
         * Uses supplied number of bits to read
         * only them.
         * @tparam T Type which should be read.
         * @param bits Number of bits to read.
         * @return Returns value read.
         * @warning Moves the current pointer by
         *   number of read bits.
         * @throws std::runtime_error when there
         *   are not enough bits left.
         */
        template <typename T>
        T readVal(std::size_t bits);

        /**
         * Read value from the current position.
         * Uses supplied number of bits to read
         * only them. Uses output memory.
         * @tparam T Type which should be read.
         * @param bits Number of bits to read.
         * @param ptr Pointer to the target output
         *   memory.
         * @return Returns value read.
         * @warning Moves the current pointer by
         *   number of read bits.
         * @throws std::runtime_error when there
         *   are not enough bits left.
         */
        template <typename T>
        void readVal(std::size_t bits, T *ptr);

        /**
         * Write a value to the current position.
         * @tparam T Type which should be written.
         * @param val Value to be written.
         * @warning Moves the current pointer by
         *   number of read bits.
         * @throws std::runtime_error when there
         *   are not enough bits left.
         */
        template <typename T>
        void writeVal(const T &val);

        /**
         * Write a value to the current position.
         * Writes only given number of lower bits.
         * @tparam T Type which should be written.
         * @param val Value to be written.
         * @param bits Number of lower bits to write.
         * @warning Moves the current pointer by
         *   number of read bits.
         * @throws std::runtime_error when there
         *   are not enough bits left.
         */
        template <typename T>
        void writeVal(const T &val, std::size_t bits);

        /// Get the vector containing data chunks.
        const std::vector<Chunk> &data() const
        { return mData; }

        /// Number of used chunks.
        std::size_t chunks() const
        { return mTotalChunks; }

        /// Get number of free bits remaining in the data buffer.
        std::size_t availableBits() const;

        /// Get number of used bits after current poisition in the data buffer.
        std::size_t availableUsedBits() const;

        /// Get number of unused bits after current poisition in the data buffer.
        std::size_t availableUnusedBits() const;
    private:
        /**
         * Get next multiple of ALLOC_SIZE chunks.
         * @param numChunks Target number of chunks.
         * @return Returns nearest upper multiple of ALLOC_SIZE chunks.
         */
        static constexpr std::size_t nextMultipleOfChunks(std::size_t numChunks)
        {
            return (numChunks % ALLOC_SIZE == 0) ? numChunks :
                   (numChunks / ALLOC_SIZE + 1) * ALLOC_SIZE;
        }

        /**
         * Allocate at least given number of chunks.
         * @param numChunks Number of chunks which should be allocated.
         * @warning Will allocate chunks in multiples of ALLOC_SIZE!
         */
        void allocateAtLeast(std::size_t numChunks);

        /**
         * Ensure there is at least given number of
         * bits between current position and the end
         * of the data buffer.
         * @param bits Number of bits we require.
         * @warning May trigger allocation of more chunks.
         */
        void ensureSpace(std::size_t bits);

        /// Vector containing the current data.
        std::vector<Chunk> mData;
        /// Index of the current chunk.
        std::size_t mCurrentChunk;
        /// Index of the current bit in the current chunk.
        std::size_t mCurrentBit;
        /// Total number of chunks in the data buffer.
        std::size_t mTotalChunks;
        /// Total number of bits used in the last chunk.
        std::size_t mLastBits;
    protected:
    }; // class PackedBitStream

    // Template implementation.

    template<typename T>
    T PackedBitStream::readVal()
    {
        return readVal<T>(sizeof(T) * BITS_PER_BYTE);
    }

    template<typename T>
    T PackedBitStream::readVal(std::size_t bits)
    {
        T target;
        readVal<T>(bits, &target);
        return target;
    }

    template<typename T>
    void PackedBitStream::readVal(std::size_t bits, T *ptr)
    {
        if (availableUsedBits() < bits)
        {
            throw std::runtime_error("Not enough bits to read requested value!");
        }

        copyBits(mData.data(), mCurrentChunk * CHUNK_BITS + mCurrentBit,
                 ptr, 0, bits);
        std::size_t finalBits = mCurrentChunk * CHUNK_BITS + mCurrentBit + bits;
        mCurrentChunk = finalBits / CHUNK_BITS;
        mCurrentBit = finalBits % CHUNK_BITS;
    }

    template<typename T>
    void PackedBitStream::writeVal(const T &val)
    { writeVal<T>(val, sizeof(T) * BITS_PER_BYTE); }

    template<typename T>
    void PackedBitStream::writeVal(const T &val, std::size_t bits)
    {
        ensureSpace(bits);

        Util::copyBits(&val, 0, mData.data(),
                       mCurrentChunk * CHUNK_BITS + mCurrentBit, bits);
        std::size_t finalBits = mCurrentChunk * CHUNK_BITS + mCurrentBit + bits;
        mCurrentChunk = finalBits / CHUNK_BITS;
        mCurrentBit = finalBits % CHUNK_BITS;
        if (mCurrentChunk >= mTotalChunks)
        {
            mTotalChunks = mCurrentChunk + 1;
            mLastBits = mCurrentBit;
        }
        /*
        if (mCurrentChunk == mTotalChunks - 1 && mCurrentBit >= mLastBits)
        {
            mLastBits = mCurrentBit;
        }
         */
    }

    // End of template implementation.
} // namespace Util

#endif //ZRE_LPC_CODEC_PACKEDBITSTREAM_H
