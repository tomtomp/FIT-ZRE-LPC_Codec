/**
 * @file Coder.h
 * @author Tomas Polasek
 * @brief Main file for the encoding program.
 */

#ifndef ZRE_LPC_CODEC_CODER_H
#define ZRE_LPC_CODEC_CODER_H

#include "Types.h"
#include "Util.h"
#include "Config.h"

#include "PackedBitStream.h"
#include "LpcCoder.h"
#include "LpcDecoder.h"

/// Application class.
class CoderApp
{
public:
    /**
     * Initialize the coding application.
     * @param argc Number of command line arguments.
     * @param argv Command line argument vector.
     */
    CoderApp(int argc, const char *argv[]);

    /**
     * Main loop of the application.
     * @return Returns application return code.
     */
    int run();
private:
    enum
    {
        /// Command line argument location for input filename.
        INPUT_ARG_LOC = 1,
        /// Command line argument location for output filename.
        OUTPUT_ARG_LOC = 2,
        /// Minimal number of command line arguments.
        MIN_ARGS
    };

    /// Help message for the application.
    static constexpr const char *HELP_MSG{"Usage: \n"
                                              "\t./zre_code INPUT_FILE OUTPUT_FILE\n"
                                              "Description:\n"
                                              "\tINPUT_FILE: Location of the input encoded file. Use \"-\" for stdin.\n"
                                              "\tOUTPUT_FILE: Location of the output file, format signed 16b numbers. Use \"-\" for stdout."};

    /// Process command line arguments.
    void processArgs(int argc, const char *argv[]);

    /**
     * Read data from given filename.
     * @param filename Filename or "-" for stdin.
     * @param output Target vector.
     */
    void readData(std::string &filename, std::vector<Sample> &output);

    /**
     * Write data to given filename.
     * @param pbs Packed bit stream.
     * @param filename Filename or "-" for stdout.
     */
    void writeData(const Util::PackedBitStream &pbs, std::string &filename);

    /// Print the help menu.
    void printHelp();

    /// Output filename.
    std::string mOutputFilename;
    /// Input data loaded from the input file.
    std::vector<Sample> mInputData;
protected:
}; // class CoderApp

#endif //ZRE_LPC_CODEC_CODER_H
