/**
 * @file Types.h
 * @author Tomas Polasek
 * @brief Common includes and types.
 */

#ifndef ZRE_LPC_CODEC_TYPES_H
#define ZRE_LPC_CODEC_TYPES_H

// Standard library includes.
#include <iostream>
#include <cstring>
#include <vector>
#include <exception>
#include <fstream>
#include <iterator>
#include <limits>
#include <algorithm>
#include <cmath>
#include <bitset>

/// Type of samples in input and output files.
using Sample = int16_t;
/// Type of samples, when floating point precision is needed.
using FpSample = float;

#endif //ZRE_LPC_CODEC_TYPES_H
