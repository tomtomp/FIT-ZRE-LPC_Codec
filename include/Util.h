/**
 * @file Util.h
 * @author Tomas Polasek
 * @brief Utility functions and classes.
 */

#ifndef ZRE_LPC_CODEC_UTIL_H
#define ZRE_LPC_CODEC_UTIL_H

#include "Types.h"

/// Namespace containing utility functions and classes.
namespace Util
{
    /**
     * Calculate number of quantized errors for given
     * frame size.
     * @param frameSize Size of the frame.
     * @param underSample How much undersampling.
     * @return Returns number of quantized errors.
     */
    static constexpr std::size_t numQuantizedErrors(std::size_t frameSize, std::size_t underSample)
    { return frameSize / underSample + ((frameSize % underSample == 0) ? 0u : 1u); }

    /**
     * Load specified stream into provided output vector.
     * Original content of output vector will be deleted!
     * @param input Input stream.
     * @param output File contents will be assigned to this vector.
     * @param preallocate Should the vector size be pre-allocated?
     * @throws std::runtime_error When error occurs.
     */
    template <typename T>
    void readIntoVector(std::istream &input, std::vector<T> &output, bool preAllocate)
    {
        // Clear any existing data in output string.
        output.clear();

        // Temporary output used for reading in the file.
        std::vector<char> outputTmp;

        if (preAllocate)
        {
            // Reserve the string size upfront.
            input.seekg(0, std::ios::end);
            outputTmp.reserve(input.tellg());
            input.seekg(0, std::ios::beg);
        }

        // Read the whole file into the temporary vector.
        outputTmp.assign(std::istreambuf_iterator<char>(input), std::istreambuf_iterator<char>());

        // How many values did we get?
        std::size_t numValues{outputTmp.size()};
        if (numValues % sizeof(T)!= 0)
        { // Check if we actually got values of requested type T.
            throw std::runtime_error("Number of bytes in given stream does not correspond to given type!");
        }

        // Copy data from temporary vector into target vector.
        output.resize(numValues / sizeof(T));
        std::memcpy(output.data(), outputTmp.data(), output.size() * sizeof(T));
    }

    /**
     * Write data in given vector to specified output stream.
     * @param data Data vector..
     * @param output Output stream.
     * @param elements Number of elements to write, 0 for all of them.
     * @throws std::runtime_error When error occurs.
     */
    template <typename T>
    void writeFromVector(const std::vector<T> &data,
                         std::ostream &output, std::size_t elements = 0)
    {
        if (elements)
        {
            output.write(reinterpret_cast<const char*>(data.data()),
                         elements * sizeof(T));
        }
        else
        {
            output.write(reinterpret_cast<const char*>(data.data()),
                         data.size() * sizeof(T));
        }
    }

    /**
     * Multiply and add vector elements.
     * @tparam T Type of the vector element.
     * @param fBeg Begin of the first vector.
     * @param fEnd End of the first vector.
     * @param sBeg Begin of the second vector.
     * @param sEnd End of the second vector.
     * @return Returns final accumulated value.
     */
    template <typename T, typename VT = typename std::vector<T>::const_iterator>
    T convolve(VT fBeg, VT fEnd, VT sBeg, VT sEnd)
    {
        auto fIt = fBeg;
        auto sIt = sBeg;
        T acc{T()};

        for (; fIt != fEnd && sIt != sEnd; ++fIt, ++sIt)
        {
            acc += *fIt * *sIt;
        }

        return acc;
    }

    /**
     * Multiply and add vector elements.
     * @tparam T Type of the vector element.
     * @param first First input.
     * @param second Second input, interchangable with the first.
     * @return Returns final accumulated value.
     */
    template <typename T>
    T convolve(const std::vector<T> &first, const std::vector<T> &second)
    {
        if (first.size() != second.size())
        {
            throw std::runtime_error("Cannot convolve 2 vectors with differing sizes!");
        }

        return convolve<T>(first.begin(), first.end(), second.begin(), second.end());
    }

    /**
     * Perform auto-correlation for given input vector.
     * @tparam T Type of the vector element.
     * @param input Input values.
     * @return Returns correlation coefficients.
     */
    template <typename T>
    std::vector<T> autoCorrelate(const std::vector<T> input)
    {
        std::vector<T> output(input.size());
        for (std::size_t iii = 0; iii < input.size(); ++iii)
        {
            output[iii] = convolve<T>(input.begin(), input.end() - iii,
                                      input.begin() + iii, input.end());
        }
        return output;
    }

    /**
     * Copy bits from one buffer to another.
     * @param src Source buffer pointer.
     * @param srcOffset Bit offset in the source buffer.
     * @param dst Destination buffer pointer
     * @param dstOffset Bit offset in the destination buffer.
     * @param size Number of bits to copy.
     */
    void copyBits(const void *src, std::size_t srcOffset,
                  void *dst, std::size_t dstOffset, std::size_t size);

    /**
     * Copy a single bit from one buffer to another.
     * @param src Source buffer pointer.
     * @param srcOffset Bit offset in the source buffer.
     * @param dst Destination buffer pointer
     * @param dstOffset Bit offset in the destination buffer.
     */
    void copyBit(const void *src, std::size_t srcOffset,
                 void *dst, std::size_t dstOffset);
} // namespace Util

#endif //ZRE_LPC_CODEC_UTIL_H

