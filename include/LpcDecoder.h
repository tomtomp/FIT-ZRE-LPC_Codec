/**
 * @file LpcDecoder.h
 * @author Tomas Polasek
 * @brief LPC decoding class.
 */

#ifndef ZRE_LPC_CODEC_LPCDECODER_H
#define ZRE_LPC_CODEC_LPCDECODER_H

#include "Types.h"
#include "Util.h"
#include "FirFilter.h"

/// Namespace containing linear predictive coding helper functions and classes.
namespace Lpc
{
    /// Abstraction for decoding data using Linear Predictive coding.
    class LpcDecoder
    {
    public:
        /**
         * Initializer the decoder with given parameters.
         * @param order Order of the LPC filter.
         */
        LpcDecoder(std::size_t order);

        /// Free all resources.
        ~LpcDecoder() = default;

        /**
         * Predict one frame of values with given LPC coefficients
         * and calculate error agains the original signal.
         * @param coeffs Contains LPC coefficients.
         * @param signal Original signal, used for error calculation.
         * @return Returns one frame worth of prediction error.
         */
        std::vector<FpSample> predictError(const std::vector<FpSample> &coeffs,
                                           const std::vector<FpSample> &signal);

        /**
         * Decode signal, using its LPC coefficients and
         * errors vector.
         * @param lpc LPC coefficients.
         * @param errors Vector of differences from original signal.
         * @param gain Gain from the original signal.
         * @return Returns reconstructed signal vector, containing
         *   same number of elements as the errors vector.
         */
        std::vector<FpSample> decode(const std::vector<FpSample> &lpc,
                                     const std::vector<FpSample> &errors,
                                     FpSample gain);
    private:
        /**
         * Continuous filter used for predicting the
         * values of signal from LPC coefficients.
         */
        Util::FirFilter mContFilter;
        Util::FirFilter mContFilter2;
        /// Order of the LPC filter.
        std::size_t mOrder;
    protected:
    }; // class LpcDecoder
} // namespace Lpc

#endif //ZRE_LPC_CODEC_LPCDECODER_H
