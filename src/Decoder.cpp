/**
 * @file Decoder.cpp
 * @author Tomas Polasek
 * @brief Main file for the decoding program.
 */

#include "Decoder.h"

int main(int argc, const char *argv[])
{
    int ret{EXIT_FAILURE};

    try {
        DecoderApp app(argc, argv);
        ret = app.run();
    } catch (const std::runtime_error &err) {
        std::cerr << "Exception: " << err.what() << std::endl;
    } catch (...) {
        std::cerr << "Unknown exception escaped the main loop!" << std::endl;
    }

    return ret;
}

DecoderApp::DecoderApp(int argc, const char **argv)
{
    processArgs(argc, argv);
}

int DecoderApp::run()
{
    Lpc::LpcDecoder decoder(LPC_ORDER);

    std::vector<Sample> outputData;
    std::vector<FpSample> synthFrameData;

    std::size_t frameCounter{0u};
    static constexpr std::size_t MIN_BITS_PER_FRAME{32};

    // Read the number of samples we should get.
    std::size_t numSamples{mPbs.readVal<std::size_t>()};
    std::cerr << "File contains " << numSamples << " samples!" << std::endl;

    while (mPbs.availableUsedBits() > MIN_BITS_PER_FRAME &&
            frameCounter * FRAME_SIZE < numSamples)
    {
        std::size_t frameStart{frameCounter * FRAME_SIZE};
        std::size_t frameEnd{frameStart + FRAME_SIZE};

        float max1 = (mPbs.readVal<uint16_t>() / LPC_MAX_MULTIPLIER);
        std::vector<FpSample> readLpc(LPC_ORDER);
        for (float &val : readLpc)
        {
            val = (mPbs.readVal<uint8_t>(LPC_BITS) / LPC_QUANT - 1.0f) * max1;
        }
        float max2 = (mPbs.readVal<uint16_t>() / ERR_MAX_MULTIPLIER);
        //max2 = 32000.0f;
        std::vector<FpSample> readUsErrors(
                Util::numQuantizedErrors(FRAME_SIZE, ERROR_UNDERSAMPLE));
        for (float &val : readUsErrors)
        {
            val = (mPbs.readVal<uint16_t>(ERR_BITS) / ERR_QUANT - 1.0f) * max2;
        }
        // Undo undersampling.
        std::vector<FpSample> readErrors(FRAME_SIZE);
        for (std::size_t iii = 0; iii < readErrors.size(); ++iii)
        {
            readErrors[iii] = readUsErrors[iii / ERROR_UNDERSAMPLE];
        }

        // Unused.
        FpSample gain{0.0f};
        // Re-synthesise the signal.
        synthFrameData = decoder.decode(readLpc, readErrors, gain);

        /*
        for(std::size_t iii = FRAME_SIZE - 1; iii > 0; --iii)
        { // De-emphasis.
            synthFrameData[iii] = ALPHA * synthFrameData[iii] +
                                  (1.0f - ALPHA) * synthFrameData[iii - 1];
        }
         */

        outputData.resize(outputData.size() + FRAME_SIZE);
        for (std::size_t iii = frameStart; iii < frameEnd; ++iii)
        { // Fill the ouput buffer with re-synthesised signal.
            outputData[iii] = synthFrameData[iii - frameStart];
        }

        frameCounter++;
    }


    // Crop any unneeded samples.
    outputData.resize(numSamples);

    // Undo the input undersampling.
    std::vector<Sample> resampledData(numSamples * SIGNAL_UNDERSAMPLE);
    for (std::size_t iii = 0; iii < resampledData.size(); ++iii)
    {
        resampledData[iii] = outputData[iii / SIGNAL_UNDERSAMPLE];
    }

    for(std::size_t iii = resampledData.size() - 1; iii > 0; --iii)
    { // De-emphasis.
        resampledData[iii] = ALPHA * resampledData[iii] +
                              (1.0f - ALPHA) * resampledData[iii - 1];
    }

    std::cerr << "Saved " << resampledData.size() << " re-sampled samples!" << std::endl;
    writeData(resampledData, mOutputFilename);
    return EXIT_SUCCESS;
}

void DecoderApp::processArgs(int argc, const char **argv)
{
    if (argc < MIN_ARGS)
    {
        printHelp();
        throw std::runtime_error("Required arguments have not been specified!");
    }

    std::string inputFilename = argv[INPUT_ARG_LOC];
    readData(inputFilename, mPbs);

    mOutputFilename = argv[OUTPUT_ARG_LOC];
    if (mOutputFilename != "-")
    { // Using normal file.
        std::ofstream outFile(mOutputFilename);
        if (!outFile.is_open())
        {
            throw std::runtime_error(std::string("Error, unable to open output file \"") + mOutputFilename + "\"");
        }
    }
}

void DecoderApp::printHelp()
{
    std::cerr << HELP_MSG << std::endl;
}

void DecoderApp::readData(std::string &filename, Util::PackedBitStream &pbs)
{
    if (filename == "-")
    { // Using stdin.
        pbs.loadFrom(std::cin, false);
    }
    else
    { // Normal file.
        std::ifstream inFile(filename, std::ios::binary);
        if (!inFile.is_open())
        {
            throw std::runtime_error(std::string("Error, unable to open input file \"") + filename + "\"");
        }
        pbs.loadFrom(inFile, true);
    }
}

void DecoderApp::writeData(const std::vector<Sample> &data, std::string &filename)
{
    if (filename == "-")
    { // Using stdin.
        Util::writeFromVector(data, std::cout);
    }
    else
    { // Normal file.
        std::ofstream outFile(mOutputFilename, std::ios::binary);
        if (!outFile.is_open())
        {
            throw std::runtime_error(std::string("Error, unable to open output file \"") + mOutputFilename + "\"");
        }
        Util::writeFromVector(data, outFile);
    }
}
