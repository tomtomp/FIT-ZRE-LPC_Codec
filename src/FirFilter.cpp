/**
 * @file FirFilter.h
 * @author Tomas Polasek
 * @brief FIR filter wrapper.
 */

#include "FirFilter.h"

#include "Config.h"

namespace Util
{
    FirFilter::FirFilter(const std::vector<FpSample> &coeffs)
    { setCoeffs(coeffs); resetWorkBuffer(); }

    FirFilter::FirFilter(std::size_t order)
    { mCoeffs.resize(order, 0); resetWorkBuffer(); }

    std::vector<FpSample> FirFilter::filter(const std::vector<FpSample> &input)
    {
        if (input.size() < order())
        {
            throw std::runtime_error("Input size of FIR filter "
                                     "should be at least the order of it!");
        }

        // Fill the starting values
        std::reverse_copy(input.begin(), input.begin() + order(), mWork.begin());

        return doFilter(input);
    }

    std::vector<FpSample> FirFilter::filterNoReset(const std::vector<FpSample> &input)
    {
        if (input.size() < order())
        {
            throw std::runtime_error("Input size of FIR filter "
                                             "should be at least the order of it!");
        }

        return doFilterNoReset(input);
    }

    std::vector<FpSample> FirFilter::filterNoResetAdd(const std::vector<FpSample> &input)
    {
        if (input.size() < order())
        {
            throw std::runtime_error("Input size of FIR filter "
                                             "should be at least the order of it!");
        }

        return doFilterNoResetAdd(input);
    }

    std::vector<FpSample> FirFilter::doFilter(const std::vector<FpSample> &input)
    {
        // Output vector, used for storing the filtered values.
        std::vector<FpSample> output(input.size() - order());

        // Filter the input signal.
        for (std::size_t iii = order(); iii < input.size(); ++iii)
        {
            // Calculate filter output and save it.
            //output[iii - order()] = Util::convolve(mWork, mCoeffs);
            output[iii] = 0.0f;
            for(std::size_t jjj = 0; jjj < order(); jjj++)
                output[iii] += mWork[jjj] * mCoeffs[jjj];
            // Move the work vector by one.
            std::copy_backward(mWork.begin(), mWork.begin() + order() - 1, mWork.end());
            // Add new value to it.
            mWork[0] = input[iii];
        }

        return output;
    }

    std::vector<FpSample> FirFilter::doFilterNoReset(const std::vector<FpSample> &input)
    {
        // Output vector, used for storing the filtered values.
        std::vector<FpSample> output(input.size());

        // Filter the input signal.
        for (std::size_t iii = 0; iii < input.size(); ++iii)
        {
            // Calculate filter output and save it.
            //output[iii] = Util::convolve(mWork, mCoeffs) + input[iii];
            output[iii] = 0.0f;
            for(std::size_t jjj = 0; jjj < order(); jjj++)
                output[iii] += mWork[jjj] * mCoeffs[jjj];
            // Move the work vector by one.
            std::copy_backward(mWork.begin(), mWork.begin() + order() - 1, mWork.end());
            // Add new value to it.
            mWork[0] = input[iii];
            //mWork[0] = output[iii];
        }

        return output;
    }

    std::vector<FpSample> FirFilter::doFilterNoResetAdd(const std::vector<FpSample> &input)
    {
        // Output vector, used for storing the filtered values.
        std::vector<FpSample> output(input.size());

        // Filter the input signal.
        for (std::size_t iii = 0; iii < input.size(); ++iii)
        {
            // Calculate filter output and save it.
            //output[iii] = Util::convolve(mWork, mCoeffs) + input[iii];
            output[iii] = 0.0f;
            for(std::size_t jjj = 0; jjj < order(); jjj++)
                output[iii] += mWork[jjj] * mCoeffs[jjj];
            output[iii] += input[iii];
            // Move the work vector by one.
            std::copy_backward(mWork.begin(), mWork.begin() + order() - 1, mWork.end());
            // Add new value to it.
            mWork[0] = output[iii];
        }

        return output;
    }

    void FirFilter::resetWorkBuffer()
    {
        mWork.resize(order(), 0);
    }

    void FirFilter::setCoeffs(const std::vector<FpSample> &coeffs)
    {
        mCoeffs = coeffs;
        if (mCoeffs.size() != mWork.size())
        {
            resetWorkBuffer();
        }
    }

    std::vector<FpSample> FirFilter::calcErrors(const std::vector<FpSample> &signal)
    {
        // Output vector, used for storing the filtered values.
        std::vector<FpSample> errors(signal.size());

        // Filter the input signal.
        for (std::size_t iii = 0; iii < signal.size(); ++iii)
        {
            // Calculate filter output and save it.
            //output[iii] = Util::convolve(mWork, mCoeffs) + input[iii];
            FpSample output = 0.0f;
            for(std::size_t jjj = 0; jjj < order(); jjj++)
                output += mWork[jjj] * mCoeffs[jjj];
            errors[iii] = signal[iii] - output;
            //FpSample quantError = (((static_cast<uint16_t>((errors[iii] / max + 1.0f) * ERR_QUANT) & 0b111) / ERR_QUANT - 1.0f) * max);
            FpSample quantError = errors[iii];
            output += quantError;
            //output += errors[iii];
            // Move the work vector by one.
            std::copy_backward(mWork.begin(), mWork.begin() + order() - 1, mWork.end());
            // Add new value to it.
            mWork[0] = output;
        }

        return errors;
    }
}
