/**
 * @file LpcCoder.cpp
 * @author Tomas Polasek
 * @brief LPC coding class.
 */

#include "LpcCoder.h"

namespace Lpc
{
    LpcCoder::LpcCoder(std::size_t order) :
        mOrder{order}
    { }

    FpSample LpcCoder::encode(const std::vector<FpSample> &input,
                              std::vector<FpSample> &coeffs)
    {
        // Auto-correlation coefficients.
        auto ac = Util::autoCorrelate(input);

        // Destination for the last error of Levinson-Durbin.
        FpSample error;
        // Calculate LPC coefficients.
        coeffs = levinsonDurbinLpc(ac, mOrder, error);

        // TODO - What?
        for (FpSample &coeff : coeffs)
        {
            coeff *= -1.0f;
        }

        // Return gain.
        return std::sqrt(error / input.size());
    }

    std::vector<FpSample> LpcCoder::levinsonDurbinLpc(const std::vector<FpSample> &correls,
                                                      std::size_t order,
                                                      FpSample &lastError)
    {
        // Prepare output array.
        std::vector<FpSample> output(order, 0);
        // Reflection coefficients.
        std::vector<FpSample> refl(order, 0);
        // Prepare work array.
        std::vector<FpSample> work(order, 0);

        // Error starts as the first coefficient.
        FpSample error = correls[0];

        // Each iteration adds one new LPC coefficient, we want 'order' of them.
        for (std::size_t iter = 1; iter <= order; ++iter)
        {
            FpSample acc{correls[iter]};
            for (std::size_t iii = 1; iii < iter; ++iii)
            {
                acc += output[iii - 1] * correls[iter - iii];
            }
            output[iter - 1] = -acc / error;
            refl[iter - 1] = -acc / error;

            error = (FpSample{1} - refl[iter - 1] * refl[iter - 1]) * error;

            for (std::size_t iii = 1; iii < iter; ++iii)
            {
                work[iii - 1] = output[iii - 1] + refl[iter - 1] * output[iter - iii - 1];
            }

            for (std::size_t iii = 1; iii < iter; ++iii)
            {
                output[iii - 1] = work[iii - 1];
            }
        }

        lastError = error;

        return output;
    }
}
