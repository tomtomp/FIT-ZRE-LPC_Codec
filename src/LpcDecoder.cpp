/**
 * @file LpcDecoder.cpp
 * @author Tomas Polasek
 * @brief LPC decoding class.
 */

#include "LpcDecoder.h"

#include "Config.h"

namespace Lpc
{
    LpcDecoder::LpcDecoder(std::size_t order) :
        mContFilter(order), mContFilter2(order), mOrder{order}
    { }

    std::vector<FpSample> LpcDecoder::predictError(const std::vector<FpSample> &coeffs,
                                                   const std::vector<FpSample> &signal)
    {
        // Prepare the filter with given coefficients
        //Util::FirFilter filter(coeffs);

        // Set new filter coefficients.
        mContFilter.setCoeffs(coeffs);
        mContFilter2.setCoeffs(coeffs);

        /*
        std::vector<FpSample> usSignal(signal.size());
        for (std::size_t iii = 0; iii < signal.size() / ERROR_UNDERSAMPLE; ++iii)
        {
            for (std::size_t jjj = 0; jjj < ERROR_UNDERSAMPLE; ++jjj)
            {
                usSignal[iii * ERROR_UNDERSAMPLE + jjj] = signal[iii * ERROR_UNDERSAMPLE];
            }
        }
         */

        /*
        // Filter the signal.
        auto filteredSignal = mContFilter2.filterNoReset(signal);
        //auto filteredSignal = filter.filter(signal);

        FpSample max{0.0f};

        // Calculate errors.
        //for (std::size_t iii = mContFilter.order(); iii < filteredSignal.size(); ++iii)
        for (std::size_t iii = 0; iii < filteredSignal.size(); ++iii)
        {
            //FpSample error = signal[iii] - filteredSignal[iii - mContFilter.order()];
            FpSample error = signal[iii] - filteredSignal[iii];
            max = std::max(max, error);
        }
        */

        return mContFilter.calcErrors(signal);
    }

    std::vector<FpSample> LpcDecoder::decode(const std::vector<FpSample> &lpc,
                                             const std::vector<FpSample> &errors,
                                             FpSample gain)
    {
        // Set new filter coefficients.
        mContFilter.setCoeffs(lpc);

        // Filter the signal.
        auto filteredSignal = mContFilter.filterNoResetAdd(errors);

        if (filteredSignal.size() != errors.size())
        {
            throw std::runtime_error("Filtered signal size is not "
                                     "the same as the number of errors!");
        }

        /*
        for (FpSample &val : filteredSignal)
        {
            if (val > 0)
            {
                val += gain;
            }
            else
            {
                val -= gain;
            }
        }
         */

        return filteredSignal;
    }
}
