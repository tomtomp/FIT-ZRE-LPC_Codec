/**
 * @file Coder.cpp
 * @author Tomas Polasek
 * @brief Main file for the encoding program.
 */

#include "Coder.h"

int main(int argc, const char *argv[])
{
    int ret{EXIT_FAILURE};

    try {
        CoderApp app(argc, argv);
        ret = app.run();
    } catch (const std::runtime_error &err) {
        std::cerr << "Exception: " << err.what() << std::endl;
    } catch (...) {
        std::cerr << "Unknown exception escaped the main loop!" << std::endl;
    }

    return ret;
}

CoderApp::CoderApp(int argc, const char **argv)
{
    processArgs(argc, argv);
}

int CoderApp::run()
{
    std::cerr << "Loaded " << mInputData.size() << " samples!" << std::endl;

    for(std::size_t iii = mInputData.size() - 1; iii > 0; --iii)
    { // Pre-emphasis.
        mInputData[iii] = mInputData[iii] - ALPHA * mInputData[iii - 1];
    }

    std::vector<Sample> inputData(mInputData.size() / SIGNAL_UNDERSAMPLE);
    for (std::size_t iii = 0; iii < mInputData.size() / SIGNAL_UNDERSAMPLE; ++iii)
    { // Under-sample
        //inputData[iii] = mInputData[iii * SIGNAL_UNDERSAMPLE];
        FpSample acc{0.0f};
        for (std::size_t jjj = 0; jjj < SIGNAL_UNDERSAMPLE; ++jjj)
        {
            acc += mInputData[iii * SIGNAL_UNDERSAMPLE + jjj];
        }
        inputData[iii] = acc / SIGNAL_UNDERSAMPLE;
    }

    std::cerr << "Under-sampled to " << inputData.size() << " samples!" << std::endl;

    std::vector<Sample> outputData(inputData.size());
    const std::size_t numSamples{inputData.size()};
    std::vector<FpSample> frameData(FRAME_SIZE);
    std::vector<FpSample> synthFrameData(FRAME_SIZE);

    Lpc::LpcCoder coder(LPC_ORDER);
    Lpc::LpcDecoder codDecoder(LPC_ORDER);
    Lpc::LpcDecoder decoder(LPC_ORDER);
    std::vector<FpSample> lpc(LPC_ORDER);
    std::vector<FpSample> errors(FRAME_SIZE);

    Util::PackedBitStream pbs;

    // Write number of samples, so we know on the decoding side.
    pbs.writeVal<std::size_t>(inputData.size());

    for (std::size_t frameStart = 0; frameStart < numSamples; frameStart += FRAME_SIZE)
    {
        const std::size_t frameEnd{std::min(frameStart + FRAME_SIZE, numSamples)};
        for (std::size_t iii = 0; iii < FRAME_SIZE; ++iii)
        {
            if (iii < frameEnd - frameStart)
            { // Real data.
                frameData[iii] = inputData[frameStart + iii];
            }
            else
            { // Filler.
                frameData[iii] = 0.0f;
            }
        }

        /*
        for(std::size_t iii = FRAME_SIZE - 1; iii > 0; --iii)
        { // Pre-emphasis.
            frameData[iii] = frameData[iii] - ALPHA * frameData[iii - 1];
            //frameData[iii] = (1.0f - ALPHA) * frameData[iii] + ALPHA * frameData[iii - 1];
        }
         */

        // Calculate LPC coefficients and error rates.
        auto gain = coder.encode(frameData, lpc);
        ((void*)(&gain));

        float max1{0.0f};
        float max2{0.0f};

        std::vector<FpSample> quantizedLpc(lpc.size());

        { // Quantize LPC coefficients.
            float max{0.0f};
            for (float &val : lpc)
            {
                max = std::max(std::abs(val), max);
            }
            //max1 = static_cast<uint16_t>(max * LPC_MAX_MULTIPLIER) / LPC_MAX_MULTIPLIER;
            max1 = max;
            pbs.writeVal<uint16_t>(max1 * LPC_MAX_MULTIPLIER);
            for (std::size_t iii = 0; iii < quantizedLpc.size(); ++iii)
            {
                //val = static_cast<int>(val / max * 255.0f) / 255.0f * max;
                quantizedLpc[iii] = static_cast<uint16_t>((lpc[iii] / max1 + 1.0f) * LPC_QUANT);
                pbs.writeVal<uint8_t>(quantizedLpc[iii], LPC_BITS);
            }
        }

        /*
        for (std::size_t iii = 0; iii < quantizedLpc.size(); ++iii)
        {
            quantizedLpc[iii] = (static_cast<uint16_t>((lpc[iii] / max1 + 1.0f) * 127.5f)
                                 / 127.5f - 1.0f) * max1;
        }
         */
        std::vector<FpSample> dequantizedLpc(quantizedLpc.size());
        for (std::size_t iii = 0; iii < dequantizedLpc.size(); ++iii)
        {
            dequantizedLpc[iii] = (quantizedLpc[iii] / LPC_QUANT - 1.0f) * max1;
        }

        errors = codDecoder.predictError(dequantizedLpc, frameData);
        if (errors.size() != FRAME_SIZE)
        {
            throw std::runtime_error("Errors does NOT have the same size as the frame!");
        }

        /**
         * Vector of quantized error coefficients.
         * Scaled original size by undersampling coefficient.
         * +1 if there are tail elements.
         */
        std::size_t numQuantErrors{
                Util::numQuantizedErrors(FRAME_SIZE, ERROR_UNDERSAMPLE)};
        std::vector<FpSample> quantizedErrors(numQuantErrors, 0.0f);

        //if (frameStart != 0)
        { // Quantize errors.
            // Undersample.
            for (std::size_t iii = 0; iii < errors.size() / ERROR_UNDERSAMPLE;
                 iii += ERROR_UNDERSAMPLE)
            {
                // Sum.
                for (std::size_t jjj = 0; jjj < ERROR_UNDERSAMPLE; ++jjj)
                {
                    quantizedErrors[iii] += errors[iii * ERROR_UNDERSAMPLE + jjj];
                }
                // Average.
                quantizedErrors[iii] /= ERROR_UNDERSAMPLE;
            }
            // Where does the tail section start at?
            std::size_t tailStart{(errors.size() / ERROR_UNDERSAMPLE) * ERROR_UNDERSAMPLE};
            // Number of elements in the tail section.
            std::size_t tailSize{errors.size() - tailStart};
            if (tailSize)
            {
                for (std::size_t iii = tailStart; iii < errors.size(); ++iii)
                {
                    quantizedErrors.back() += errors[iii];
                }
                quantizedErrors.back() /= tailSize;
            }
            float max{0.0f};
            for (float &val : quantizedErrors)
            {
                max = std::max(std::abs(val), max);
            }
            max2 = max;
            //max2 = 32000.0f;
            pbs.writeVal<uint16_t>(max2 * ERR_MAX_MULTIPLIER);
            for (FpSample &val : quantizedErrors)
            {
                //val = static_cast<int>(val / max * 8.0f) / 8.0f * max;
                val = static_cast<uint16_t>((val / max2 + 1.0f) * ERR_QUANT);
                pbs.writeVal<uint16_t>(val, ERR_BITS);
            }
        }

        /*
        pbs.seekStart();
        float max3 = (pbs.readVal<uint16_t>() / LPC_MAX_MULTIPLIER);
        ((void*)(&max3));
        std::vector<FpSample> readLpc(LPC_ORDER);
        for (float &val : readLpc)
        {
            val = (pbs.readVal<int>(LPC_BITS) / LPC_QUANT - 1.0f) * max3;
        }
        float max4 = (pbs.readVal<uint16_t>() / ERR_MAX_MULTIPLIER);
        ((void*)(&max4));
        std::vector<FpSample> readUsErrors(
                Util::numQuantizedErrors(FRAME_SIZE, ERROR_UNDERSAMPLE));
        for (float &val : readUsErrors)
        {
            val = (pbs.readVal<int>(ERR_BITS) / ERR_QUANT - 1.0f) * max4;
        }
        // Undo undersampling.
        std::vector<FpSample> readErrors(FRAME_SIZE);
        for (std::size_t iii = 0; iii < readErrors.size(); ++iii)
        {
            readErrors[iii] = readUsErrors[iii / ERROR_UNDERSAMPLE];
        }

        FpSample err1Sum{0.0f};
        for (FpSample val : errors)
        {
            err1Sum += std::abs(val);
        }
        FpSample err2Sum{0.0f};
        for (FpSample val : errors2)
        {
            err2Sum += std::abs(val);
        }
        FpSample err3Sum{0.0f};
        for (FpSample val : readErrors)
        {
            err3Sum += std::abs(val);
        }

        FpSample err4Sum{0.0f};
        for (FpSample val : readErrors)
        {
            err4Sum += std::abs(val);
        }

        // Re-synthesise the signal.
        synthFrameData = decoder.decode(readLpc, readErrors, gain);
        //lpc_synthesize(t, synthFrameData.data(), FRAME_SIZE, readLpc.data(), LPC_ORDER, 0.0f, 0.0f, readErrors.data());

        FpSample origEnergy{0.0f};
        for (FpSample val : frameData)
        {
            origEnergy += val * val;
        }
        origEnergy /= frameData.size();

        FpSample synthEnergy{0.0f};
        for (FpSample val : synthFrameData)
        {
            synthEnergy += val * val;
        }
        synthEnergy /= synthFrameData.size();
        float power2 = sqrt(err4Sum) / synthFrameData.size();
        ((void*)(&power2));

        for(int iii = FRAME_SIZE - 1; iii > 0; --iii)
        { // De-emphasis.
            synthFrameData[iii] = ALPHA * synthFrameData[iii] +
                    (1.0f - ALPHA) * synthFrameData[iii - 1];
        }

        for (std::size_t iii = frameStart; iii < frameEnd; ++iii)
        { // Fill the ouput buffer with re-synthesised signal.
            outputData[iii] = synthFrameData[iii - frameStart];
        }
         */
    }

    // Size in bits of the original data.
    std::size_t origSize{mInputData.size() * sizeof(int16_t) * 8};
    // Size in bits of the compressed data.
    std::size_t compressedSize{pbs.chunks() * Util::PackedBitStream::CHUNK_BITS};
    // Print out statistics about the compression.
    std::cerr << "Compression : " << origSize << " bits -> "
              << compressedSize << " bits . \nFactor: "
              << static_cast<float>(origSize) / compressedSize << std::endl;

    //writeData(outputData, mOutputFilename);
    writeData(pbs, mOutputFilename);

    return EXIT_SUCCESS;
}

void CoderApp::processArgs(int argc, const char **argv)
{
    if (argc < MIN_ARGS)
    {
        printHelp();
        throw std::runtime_error("Required arguments have not been specified!");
    }

    std::string inputFilename = argv[INPUT_ARG_LOC];
    readData(inputFilename, mInputData);

    mOutputFilename = argv[OUTPUT_ARG_LOC];
    if (mOutputFilename != "-")
    { // Using normal file.
        std::ofstream outFile(mOutputFilename);
        if (!outFile.is_open())
        {
            throw std::runtime_error(std::string("Error, unable to open output file \"") + mOutputFilename + "\"");
        }
    }
}

void CoderApp::printHelp()
{
    std::cerr << HELP_MSG << std::endl;
}

void CoderApp::readData(std::string &filename, std::vector<Sample> &output)
{
    if (filename == "-")
    { // Using stdin.
        Util::readIntoVector(std::cin, output, false);
    }
    else
    { // Normal file.
        std::ifstream inFile(filename, std::ios::binary);
        if (!inFile.is_open())
        {
            throw std::runtime_error(std::string("Error, unable to open input file \"") + filename + "\"");
        }
        Util::readIntoVector(inFile, output, true);
    }
}

void CoderApp::writeData(const Util::PackedBitStream &pbs, std::string &filename)
{
    if (filename == "-")
    { // Using stdin.
        Util::writeFromVector(pbs.data(), std::cout, pbs.chunks());
    }
    else
    { // Normal file.
        std::ofstream outFile(mOutputFilename, std::ios::binary);
        if (!outFile.is_open())
        {
            throw std::runtime_error(std::string("Error, unable to open output file \"") + mOutputFilename + "\"");
        }
        Util::writeFromVector(pbs.data(), outFile, pbs.chunks());
    }
}
