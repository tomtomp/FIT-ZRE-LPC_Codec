/**
 * @file Util.cpp
 * @author Tomas Polasek
 * @brief Utility functions and classes.
 */

#include "Util.h"

namespace Util
{
    void copyBits(const void *src, std::size_t srcOffset,
                  void *dst, std::size_t dstOffset, std::size_t size)
    {
        // TODO - Rework, if time permits...
        /*
        // Number of bits per one byte.
        static constexpr std::size_t BITS_PER_BYTE{8};

        // Source byte buffer pointer.
        auto srcDat = static_cast<uint8_t*>(src);
        // How many full bytes should we skip?
        std::size_t srcByteOffset{srcOffset / BITS_PER_BYTE};
        // How many bits should we skip after the full bytes?
        std::size_t srcBitOffset{srcOffset % BITS_PER_BYTE};

        // Destination byte buffer pointer.
        auto dstDat = static_cast<uint8_t*>(src);
        // How many full bytes should we skip?
        std::size_t dstByteOffset{dstOffset / BITS_PER_BYTE};
        // How many bits should we skip after the full bytes?
        std::size_t dstBitOffset{dstOffset % BITS_PER_BYTE};

        // How many full bytes do we need to copy?
        std::size_t bytesToCopy{size / BITS_PER_BYTE};
        // How many bits do we need to copy after the full bytes?
        std::size_t bitsToCopy{size % BITS_PER_BYTE};

        if (srcOffset == dstOffset)
        { // We can copy most of the array as bytes.

        }
        else
        { // We need to copy bit by bit.

        }
         */

        std::size_t srcIt{srcOffset};
        std::size_t dstIt{dstOffset};
        std::size_t dstEnd{dstOffset + size};

        for (; dstIt != dstEnd; ++srcIt, ++ dstIt)
        {
            copyBit(src, srcIt, dst, dstIt);
        }
    }

    void copyBit(const void *src, std::size_t srcOffset,
                 void *dst, std::size_t dstOffset)
    {
        // Number of bits per one byte.
        static constexpr std::size_t BITS_PER_BYTE{8};

        // Source byte buffer pointer.
        auto srcDat = static_cast<const uint8_t*>(src);
        // How many full bytes should we skip?
        std::size_t srcByteOffset{srcOffset / BITS_PER_BYTE};
        // How many bits should we skip after the full bytes?
        std::size_t srcBitOffset{srcOffset % BITS_PER_BYTE};

        // Destination byte buffer pointer.
        auto dstDat = static_cast<uint8_t*>(dst);
        // How many full bytes should we skip?
        std::size_t dstByteOffset{dstOffset / BITS_PER_BYTE};
        // How many bits should we skip after the full bytes?
        std::size_t dstBitOffset{dstOffset % BITS_PER_BYTE};

        // Get the source byte, where the target bit is.
        uint8_t srcByte{srcDat[srcByteOffset]};
        // Source byte mask.
        uint8_t srcMask{static_cast<uint8_t>(1 << srcBitOffset)};

        // Get value of the bit from source.
        uint8_t srcValue = srcByte & srcMask;

        // Destination byte mask.
        uint8_t dstMask{static_cast<uint8_t>(1 << dstBitOffset)};

        // TODO - Change to XOR logic...
        if (srcValue)
        {
            dstDat[dstByteOffset] |= dstMask;
        }
        else
        {
            dstDat[dstByteOffset] &= ~dstMask;
        }
    }
}
